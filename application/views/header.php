<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Joshi Brothers ERP</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css">
	<link href="<?php echo base_url(); ?>assets/css/prettyPhoto.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" />
  </head>
  <body>
	<header>		
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="navigation">
				<div class="container">					
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse.collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<div class="navbar-brand">
							<a href="<?php echo base_url(); ?>index.php/welcome/index">
                            	<img src="<?php echo base_url(); ?>assets/images/logo_jb.jpg"/>
                            </a>
						</div>
					</div>
					<!--
					<div class="menu">
						<ul class="nav nav-tabs" role="tablist" style="text-align:right; vertical-align:top; font-weight:100;">
							<li role="presentation">
								<a href="<?php echo base_url(); ?>index.php/login">
									<b style="font-size:20px">Login <i class="fa fa-sign-in"></i></b>
								</a>
							</li>							
						</ul>
					</div>
					-->
					<div class="navbar-collapse collapse">							
						<div class="menu">
							<ul class="nav nav-tabs" role="tablist" style="text-align:right; vertical-align:top; font-weight:100">
                                <li role="presentation">
									<a href="<?php echo base_url(); ?>index.php/login">
										<b style="font-size:16px">Login <i class="fa fa-sign-in"></i></b>
									</a>
								</li>							
							</ul>
						</div>
					</div>					
				</div>
			</div>	
		</nav>		
	</header>

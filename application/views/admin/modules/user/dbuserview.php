<?php
    $user_id = $_REQUEST['id'];
    
    if($user_id != ""){
        $sql_user_det = "select * from login where id = '$user_id'";
        $qry_user_det = $this->db->query($sql_user_det)->row();
        $email = $qry_user_det->email;
        $password = $qry_user_det->password;
        $username = $qry_user_det->username;
        $name = $qry_user_det->name;
        $dob = $qry_user_det->dob;
        $mob_no = $qry_user_det->mob_no;
        $role = $qry_user_det->role;
        $emp_active = $qry_user_det->emp_active;
        $dept = $qry_user_det->dept;
        $emp_comp = $qry_user_det->emp_comp;
    } else {
        $email = "";
        $password = "";
        $username = "";
        $name = "";
        $dob = "";
        $mob_no = "";
        $role = "";
        $emp_active = "";
        $dept = "";  
        $emp_comp = "";
    }
?>
<section id="main-content">
  <section class="wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>User Register</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div> 
    </div>
    <form action="<?php echo base_url(); ?>index.php/dbuserc/RegisterUser" method="post">
        <!-- Hidden Id's -->
        <input type="hidden" id="id" name="id" value="<?php echo $user_id; ?>">

        <div class="row" style="text-align:center">
            <div class="col-lg-3"></div>
            <div class="col-lg-3"><h4>Email</h4></div>
            <div class="col-lg-3">
                <input type="text" id="email" name="email" value="<?php echo $email; ?>" class="form-control" required>
            </div>
            <div class="col-lg-3"></div>   
        </div><br /><br />

        <div class="row" style="text-align:center">
            <div class="col-lg-3"></div>
            <div class="col-lg-3"><h4>Name</h4></div>
            <div class="col-lg-3">
                <input type="text" id="name" name="name" value="<?php echo $name; ?>" class="form-control" required>
            </div>
            <div class="col-lg-3"></div>   
        </div><br /><br />

        <div class="row" style="text-align:center">
            <div class="col-lg-3"></div>
            <div class="col-lg-3"><h4>Password</h4></div>
            <div class="col-lg-3"><input type="text" id="pass" name="pass" value="<?php echo $password; ?>" class="form-control" required></div>
            <div class="col-lg-3"></div>   
        </div><br /><br />

        <div class="row" style="text-align:center">
            <div class="col-lg-3"></div>
            <div class="col-lg-3"><h4>Username</h4></div>
            <div class="col-lg-3">
                <input type="text" id="username" name="username" value="<?php echo $username; ?>" class="form-control" required>
            </div>
            <div class="col-lg-3"></div>   
        </div><br /><br />

        <div class="row" style="text-align:center">
            <div class="col-lg-3"></div>
            <div class="col-lg-3"><h4>Date Of Birth</h4></div>
            <div class="col-lg-3">
                <input type="text" id="dob" name="dob" value="<?php echo $dob; ?>" class="form-control" required>
            </div>
            <div class="col-lg-3"></div>   
        </div><br /><br />

        <div class="row" style="text-align:center">
            <div class="col-lg-3"></div>
            <div class="col-lg-3"><h4>Mobile Number</h4></div>
            <div class="col-lg-3">
                <input type="text" id="mob_no" name="mob_no" value="<?php echo $mob_no; ?>" class="form-control" required>
            </div>
            <div class="col-lg-3"></div>   
        </div><br /><br />

        <div class="row" style="text-align:center">
            <div class="col-lg-3"></div>
            <div class="col-lg-3"><h4>Role</h4></div>
            <div class="col-lg-3">
                <select id="role" name="role" class="form-control" required>
                    <option value="<?php echo $role; ?>"><?php echo $role; ?></option>
                    <option value="">--select--</option>
                    <option value="Admin">Admin</option>
                    <option value="User">User</option>
                </select>
            </div>
            <div class="col-lg-3"></div>   
        </div><br /><br />

        <div class="row" style="text-align:center">
            <div class="col-lg-3"></div>
            <div class="col-lg-3"><h4>Department</h4></div>
            <div class="col-lg-3">
                <select id="dept" name="dept" class="form-control" required>
                    <option value="<?php echo $dept; ?>"><?php echo $dept; ?></option>
                    <option value="">--select--</option>
                    <option value="IT">IT</option>
                    <option value="Sales">Sales</option>
                    <option value="Director">Director</option>
                    <option value="Other">Other</option>
                </select>
            </div>
            <div class="col-lg-3"></div>   
        </div><br /><br />

        <div class="row" style="text-align:center">
            <div class="col-lg-3"></div>
            <div class="col-lg-3"><h4>Company</h4></div>
            <div class="col-lg-3">
                <select id="emp_comp" name="emp_comp" class="form-control" required>
                    <option value="<?php echo $emp_comp; ?>"><?php echo $emp_comp; ?></option>
                    <option value="">--select--</option>
                    <option value="Joshi Brothers">Joshi Brothers</option>
                </select>
            </div>
            <div class="col-lg-3"></div>   
        </div><br /><br />

        <div class="row" style="text-align:center">
            <div class="col-lg-3"></div>
            <div class="col-lg-3"><h4>Employee Active</h4></div>
            <div class="col-lg-3">
                <select id="emp_active" name="emp_active" class="form-control" required>
                    <option value="<?php echo $emp_active; ?>"><?php echo $emp_active; ?></option>
                    <option value="">---select---</option>
                    <option value="yes">yes</option>
                    <option value="no">no</option>
                </select>
            </div>
            <div class="col-lg-3"></div>   
        </div><br /><br />

        <div class="row" style="text-align:center">
            <div class="col-lg-5"></div>
            <div class="col-lg-2"><input type="submit" id="submit" name="submit" value="Submit" class="form-control"></div>
            <div class="col-lg-5"></div>   
        </div><br /><br />
    </form>

  </section>
</section>
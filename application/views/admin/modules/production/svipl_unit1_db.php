<section id="main-content">
  <section class="wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>Production Dashboard</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div> 
    </div>
    
    <div class="row" style="text-align:center">
    	<div class="col-lg-2">
        <a href="<?php echo base_url(); ?>index.php/productionc/prod_plates_list">
              <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
              Plates Production List
          </a>
      </div>

      <div class="col-lg-2">
        <a href="<?php echo base_url(); ?>index.php/productionc/chhilai_list_u1">
              <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
              Chhilai List
          </a>
      </div>

      <div class="col-lg-2">
        <a href="<?php echo base_url(); ?>index.php/productionc/color_memo_list_u1">
              <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
              Color Memo List
          </a>
      </div>

      <div class="col-lg-2">
        <a href="<?php echo base_url(); ?>index.php/productionc/bore_memo_list_u1">
              <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
              Bore Memo List
          </a>
      </div>

      <div class="col-lg-2">
        <a href="<?php echo base_url(); ?>index.php/productionc/prod_daily_plates">
              <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
              Daily Plates
          </a>
      </div>

      <div class="col-lg-2">
        <a href="<?php echo base_url(); ?>index.php/productionc/prod_daily_plates_lw">
              <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
              Daily Produce
          </a>
      </div>
      
    </div><br><br>

    <div class="row" style="text-align:center">
      <div class="col-lg-2">
        <a href="<?php echo base_url(); ?>index.php/productionc/prod_daily_plates">
              <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
              Monthly Detail
          </a>
      </div>

      <div class="col-lg-2">
        <a href="<?php echo base_url(); ?>index.php/productionc/prod_daily_plates">
              <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
              Monthly Worker
          </a>
      </div>

      <div class="col-lg-2">
        <a href="<?php echo base_url(); ?>index.php/productionc/prod_daily_plates">
              <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
              Monthly Plates
          </a>
      </div>
    </div>

  </section>
</section>
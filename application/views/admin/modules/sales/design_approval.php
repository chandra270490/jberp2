<?php $this->load->helper("finance"); ?>
<section id="main-content">
  <section class="wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>Design Approval</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div> 
    </div>
    
    <?php
        $so_id = $_REQUEST['id'];
        if($so_id != ''){
            foreach($get_by_id->result() as $row){
                $so_cust_name = $row->so_cust_name;
                $so_cust_add = $row->so_cust_add;
                $so_cust_city = $row->so_cust_city;
                $so_cust_state = $row->so_cust_state;
                $so_cust_country = $row->so_cust_country;
                $so_cust_pin = $row->so_cust_pin;
                $so_cust_email = $row->so_cust_email;
                $so_cust_phone = $row->so_cust_phone;
                $so_cust_add = $row->so_cust_add;
            }
        } else {
            $so_cust_name = "";
            $so_cust_add = "";
            $so_cust_city = "";
            $so_cust_state = "";
            $so_cust_country = "";
            $so_cust_pin = "";
            $so_cust_email = "";
            $so_cust_phone = "";
            $so_cust_add = "";
        }
    ?>

    <div class="row" style="text-align:center">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
        <section class="panel">
            <header class="panel-heading"><h4>Design Approval</h4></header>
            <form class="form-horizontal " method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>index.php/salesc/design_approval_entry">
            <div class="panel-body">
                <!--- Sale Order Details -->
                <?php include("so_details.php"); ?>

                <div class="form-group">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-3">
                        <textarea id="so_design_app_rmks" name="so_design_app_rmks" class="form-control"></textarea>
                    </div>
                    <div class="col-sm-3">
                        <input type="submit" id="approve" name="approve" value="Approve" class="form-control">
                    </div>
                    <div class="col-sm-3"></div>
                </div>  

            </form>
            </div>
        </section>
        </div>
        <div class="col-lg-1"></div>
    </div>
  </section>
</section>
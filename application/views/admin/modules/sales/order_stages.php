<?php $this->load->helper("itemlist"); ?>
<?php $this->load->helper("sales"); ?>

<style>
	button.accordion {
		background-color:#ddd;
		color: #444;
		cursor: pointer;
		padding: 5px;
		width: 100%;
		border: none;
		text-align: center;
		font-weight:bold;
		outline: none;
		font-size: 14px;
		transition: 0.4s;
		border-radius:8px;
	}
	
	button.accordion.active, button.accordion:hover {
		background-color: #999999;
	}
	
	button.accordion:after {
		content: '\02795';
		font-size: 13px;
		color: #777;
		float: right;
		margin-left: 5px;
	}
	
	button.accordion.active:after {
		content: "\2796";
	}
	
	div.panel {
		padding: 0 5px;
		background-color: white;
		max-height: 0;
		overflow: hidden;
		transition: 0.6s ease-in-out;
		opacity: 0;
		margin-bottom:4px;
	}
	
	div.panel.show {
		opacity: 1;
		max-height: 750px;
	}
	
	table thead tr{
		display:block;
	}
	
	table th,table td{
		width:300px;
	}
	
	table  tbody{		
		display:block;
		/*height:200px;*/
		overflow:auto;
	}
</style>

<section id="main-content">
  <section class="wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>Order Stages</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div> 
    </div>

    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <section class="panel">
                <header class="panel-heading" style="text-align:center; font-weight:bold">Order Stages</header>
                <!-- Accordian Starts-->
                <div class="panel-body">
                    <button class="accordion" style="color:black; font-weight:bold;" onclick="accordian();" type="button"><b>Order Booking</b></button>
                    <div class="panel" style="text-align:center"><a href="<?php echo base_url(); ?>index.php/salesc/order_form?id="><h4>Book New Order</h4></a></div>

                    <button class="accordion" style="color:black; font-weight:bold;" onclick="accordian();" type="button">
                        <b>Order Approval</b> (<?php echo so_cnt("Order Approval"); ?>)
                    </button>
                    <div class="panel"><?php echo so_det("Order Approval"); ?></div>

                    <button class="accordion" style="color:black; font-weight:bold;" onclick="accordian();" type="button">
                        <b>Design Making</b> (<?php echo so_cnt("Design Making"); ?>)
                    </button>
                    <div class="panel"><?php echo so_det("Design Making"); ?></div>

                    <button class="accordion" style="color:black; font-weight:bold;" onclick="accordian();" type="button">
                        <b>Design Approval</b> (<?php echo so_cnt("Design Approval"); ?>)
                    </button>
                    <div class="panel"><?php echo so_det("Design Approval"); ?></div>

                    <button class="accordion" style="color:black; font-weight:bold;" onclick="accordian();" type="button">
                        <b>Die Making</b> (<?php echo so_cnt("Die Making"); ?>)
                    </button>
                    <div class="panel"><?php echo so_det("Die Making"); ?></div>

                    <button class="accordion" style="color:black; font-weight:bold;" onclick="accordian();" type="button">
                        <b>Bag Making</b> (<?php echo so_cnt("Bag Making"); ?>)
                    </button>
                    <div class="panel"><?php echo so_det("Bag Making"); ?></div>

                    <button class="accordion" style="color:black; font-weight:bold;" onclick="accordian();" type="button">
                        <b>Printing</b> (<?php echo so_cnt("Printing"); ?>)
                    </button>
                    <div class="panel"><?php echo so_det("Printing"); ?></div>

                    <button class="accordion" style="color:black; font-weight:bold;" onclick="accordian();" type="button">
                        <b>Counting PCS</b> (<?php echo so_cnt("Counting PCS"); ?>)
                    </button>
                    <div class="panel"><?php echo so_det("Counting PCS"); ?></div>

                    <button class="accordion" style="color:black; font-weight:bold;" onclick="accordian();" type="button">
                        <b>Weight Counting</b> (<?php echo so_cnt("Weight Counting"); ?>)
                    </button>
                    <div class="panel"><?php echo so_det("Weight Counting"); ?></div>

                    <button class="accordion" style="color:black; font-weight:bold;" onclick="accordian();" type="button">
                        <b>Generate Bill</b> (<?php echo so_cnt("Generate Bill"); ?>)
                    </button>
                    <div class="panel"><?php echo so_det("Generate Bill"); ?></div>

                    <button class="accordion" style="color:black; font-weight:bold;" onclick="accordian();" type="button">
                        <b>Generate Bill Approval</b> (<?php echo so_cnt("Generate Bill Approval"); ?>)
                    </button>
                    <div class="panel"><?php echo so_det("Generate Bill Approval"); ?></div>

                    <button class="accordion" style="color:black; font-weight:bold;" onclick="accordian();" type="button">
                        <b>Payment</b> (<?php echo so_cnt("Payment"); ?>)
                    </button>
                    <div class="panel"><?php echo so_det("Payment"); ?></div>

                    <button class="accordion" style="color:black; font-weight:bold;" onclick="accordian();" type="button">
                        <b>Payment Approval</b> (<?php echo so_cnt("Payment Approval"); ?>)
                    </button>
                    <div class="panel"><?php echo so_det("Payment Approval"); ?></div>

                    <button class="accordion" style="color:black; font-weight:bold;" onclick="accordian();" type="button">
                        <b>Dispatch</b> (<?php echo so_cnt("Dispatch"); ?>)
                    </button>
                    <div class="panel"><?php echo so_det("Dispatch"); ?></div>

                    <button class="accordion" style="color:black; font-weight:bold;" onclick="accordian();" type="button">
                        <b>Dispatch Approval</b> (<?php echo so_cnt("Dispatch Approval"); ?>)
                    </button>
                    <div class="panel"><?php echo so_det("Dispatch Approval"); ?></div>

                    <button class="accordion" style="color:black; font-weight:bold;" onclick="accordian();" type="button">
                        <b>POD</b> (<?php echo so_cnt("POD"); ?>)
                    </button>
                    <div class="panel"><?php echo so_det("POD"); ?></div> 

                    <button class="accordion" style="color:black; font-weight:bold;" onclick="accordian();" type="button">
                        <b>Completed Orders</b> (<?php echo so_cnt("Complete"); ?>)
                    </button>
                    <div class="panel"><?php echo so_det("Complete"); ?></div>   
                </div><br/><br/>
                <!-- Accordian Ends -->
            </section>
        </div>
        <div class="col-lg-2"></div>
    </div>
  </section>
</section>

<!-- According Javascript -->
<script type="text/javascript">
    function accordian(){
        var acc = document.getElementsByClassName("accordion");
        var i;
        for (i = 0; i < acc.length; i++) {
            acc[i].onclick = function(){
                this.classList.toggle("active");
                this.nextElementSibling.classList.toggle("show");
            }
        }
    }
	
</script>
<?php $this->load->helper("finance"); ?>
<section id="main-content">
  <section class="wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>Order Booking</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div> 
    </div>
    
    <?php
        $so_id = $_REQUEST['id'];
        if($so_id != ''){
            foreach($get_by_id->result() as $row){
                $so_cust_name = $row->so_cust_name;
                $so_cust_add = $row->so_cust_add;
                $so_cust_city = $row->so_cust_city;
                $so_cust_state = $row->so_cust_state;
                $so_cust_country = $row->so_cust_country;
                $so_cust_pin = $row->so_cust_pin;
                $so_cust_email = $row->so_cust_email;
                $so_cust_phone = $row->so_cust_phone;
                $so_cust_add = $row->so_cust_add;
            }
        } else {
            $so_cust_name = "";
            $so_cust_add = "";
            $so_cust_city = "";
            $so_cust_state = "";
            $so_cust_country = "";
            $so_cust_pin = "";
            $so_cust_email = "";
            $so_cust_phone = "";
            $so_cust_add = "";
        }
    ?>

    <div class="row" style="text-align:center">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
        <section class="panel">
            <header class="panel-heading"><h4>Order Booking</h4></header>
            <form class="form-horizontal " method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>index.php/salesc/order_entry">
            <div class="panel-body">
                <?php
                    if($so_id != ''){
                        echo "<h2>Sale Order Id - ".$so_id."</h2>";
                ?>
                    <input type="hidden" id="so_id" name="so_id" value="<?=$so_id; ?>">
                <?php } else { ?>
                    <input type="hidden" id="so_id" name="so_id" value="">
                <?php } ?>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Customer Name</label>
                    <div class="col-sm-10">
                        <input type="text" id="so_cust_name" name="so_cust_name" value="<?=$so_cust_name?>"  class="form-control" required>
                    </div>
                </div>

                <div class="form-group">
                <label class="col-sm-2 control-label">Address</label>
                    <div class="col-sm-10">
                        <input type="text" id="so_cust_add" name="so_cust_add" value="<?=$so_cust_add?>"  class="form-control" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-1 control-label">City</label>
                    <div class="col-sm-3">
                        <input type="text" id="so_cust_city" name="so_cust_city" value="<?=$so_cust_city?>"  class="form-control" required>
                    </div>

                    <label class="col-sm-1 control-label">State</label>
                    <div class="col-sm-3">
                        <select id="so_cust_state" name="so_cust_state" class="form-control" required>
                            <?php if($so_id != ""){ ?> 
                                <option value="<?=$so_cust_state;?>" selected><?=$so_cust_state;?></option>
                            <?php } ?>
                                <option value="">--select--</option>
                            <?php 
                                $sql_state = "select * from state_mst order by id";
                                $qry_state = $this->db->query($sql_state);
                                foreach($qry_state->result() as $row){
                            ?>
                                <option value="<?=$row->state_name;?>"><?=$row->state_name;?></option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>

                    <label class="col-sm-1 control-label">Country</label>
                    <div class="col-sm-3">
                        <select id="so_cust_country" name="so_cust_country" class="form-control" required>
                            <?php if($so_id != ""){ ?> 
                                <option value="<?=$so_cust_country;?>" selected><?=$so_cust_country;?></option>
                            <?php } ?>
                                <option value="">--select--</option>
                            <?php 
                                $sql_country = "select * from country_mst order by id";
                                $qry_country = $this->db->query($sql_country);
                                foreach($qry_country->result() as $row){
                            ?>
                                <option value="<?=$row->country_name;?>"><?=$row->country_name;?></option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-1 control-label">Pincode</label>
                    <div class="col-sm-3">
                        <input type="text" id="so_cust_pin" name="so_cust_pin" value="<?=$so_cust_pin?>"  class="form-control" required>
                    </div>

                    <label class="col-sm-1 control-label">Email</label>
                    <div class="col-sm-3">
                        <input type="text" id="so_cust_email" name="so_cust_email" value="<?=$so_cust_email?>"  class="form-control" required>
                    </div>

                    <label class="col-sm-1 control-label">Phone</label>
                    <div class="col-sm-3">
                        <input type="text" id="so_cust_phone" name="so_cust_phone" value="<?=$so_cust_phone?>" onkeypress="return isNumberKey(event);" class="form-control" required>
                    </div>
                </div>

                <div class="form-group">
                    <br/><br/>
                    <div class="table-wrapper">
                        <div class="table-title">
                            <div class="row">
                                <div class="col-sm-12"><h3>Item Details</b></h3></div>
                            </div>
                        </div>
                        <table class="table table-bordered" id="item_tbl">
                            <thead>
                                <tr>
                                    <th>Size</th>
                                    <th>Side</th>
                                    <th>Color</th>
                                    <th>GSM</th>
                                    <th>Print Color</th>
                                    <th>Loop Color</th>
                                    <th>Price Basis</th>
                                    <th>Price</th>
                                    <th>Description</th>
                                    <th>Last Bag Picture</th>
                                    <th><span class="glyphicon glyphicon-plus" style="font-size:15px;color:green;" onclick="addrow();"></span></th>
                                </tr>
                            </thead>
                            <tbody style="text-align:left">
                                <?php
                                if($so_id != ''){
                                    $sql_itm_list = "select * from so_dtl where so_id ='".$so_id."'";
                                    $qry_itm_list = $this->db->query($sql_itm_list);

                                    $cnt = 0;
                                    foreach($qry_itm_list->result() as $row){
                                        $cnt++;
                                        $size = $row->size;
                                        $side = $row->side;
                                        $color = $row->color;
                                        $gsm = $row->gsm;
                                        $print_color = $row->print_color;
                                        $loop_color = $row->loop_color;
                                        $price_basis = $row->price_basis;
                                        $price = $row->price;
                                        $description = $row->description;
                                        $last_bag_pic = $row->last_bag_pic;
                                ?>
                                <tr>
                                    <td><?=$size;?><input type="hidden" id="size" name="size[]" value="<?=$size;?>"></td>
                                    <td><?=$side;?><input type="hidden" id="side" name="side[]" value="<?=$side;?>"></td>
                                    <td><?=$color;?><input type="hidden" id="color" name="color[]" value="<?=$color;?>"></td>
                                    <td><?=$gsm;?><input type="hidden" id="gsm" name="gsm[]" value="<?=$gsm;?>"></td>
                                    <td><?=$print_color;?><input type="hidden" id="print_color" name="print_color[]" value="<?=$print_color;?>"></td>
                                    <td><?=$loop_color;?><input type="hidden" id="loop_color" name="loop_color[]" value="<?=$loop_color;?>"></td>
                                    <td><?=$price_basis;?><input type="hidden" id="price_basis" name="price_basis[]" value="<?=$price_basis;?>"></td>
                                    <td><?=$price;?><input type="hidden" id="price" name="price[]" value="<?=$price;?>"></td>
                                    <td><?=$description;?><input type="hidden" id="description" name="description[]" value="<?=$description;?>"></td>
                                    <td><?=$last_bag_pic;?><input type="hidden" id="last_bag_pic" name="last_bag_pic[]" value="<?=$last_bag_pic;?>"></td>
                                    <td><span class="glyphicon glyphicon-remove" style="font-size:15px;color:red;" onclick="deleterow()"></span></td>
                                </tr>
                                <?php
                                    }   
                                } else {
                                ?>
                                <tr>
                                    <td><input type="text" id="size" name="size[]" value="" class="form-control" required></td>
                                    <td><input type="text" id="side" name="side[]" value="" class="form-control" required></td>
                                    <td><input type="text" id="color" name="color[]" value="" class="form-control" required></td>
                                    <td><input type="text" id="gsm" name="gsm[]" value="" class="form-control" onkeypress="return isNumberKey(event);" required></td>
                                    <td><input type="text" id="print_color" name="print_color[]" value="" class="form-control" required></td>
                                    <td><input type="text" id="loop_color" name="loop_color[]" value=""  class="form-control" required></td>
                                    <td>
                                        <select id="price_basis" name="price_basis[]" class="form-control" required>
                                            <option value="KG">KG</option>
                                            <option value="PCS">PCS</option>
                                        </select>
                                    </td>
                                    <td><input type="text" id="price" name="price[]" value="" class="form-control" onkeypress="return isNumberKey(event);" required></td>
                                    <td><input type="text" id="description" name="description[]" value="" class="form-control"></td>
                                    <td><input type="file" id="last_bag_pic" name="last_bag_pic[]" value=""  class="form-control"></td>
                                    <td><span class="glyphicon glyphicon-remove" style="font-size:15px;color:red;" onclick="deleterow()"></span></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4">
                        <input type="submit" id="submit" name="submit" value="Submit" class="form-control">
                    </div>
                    <div class="col-sm-4"></div>
                </div>
                  
            </form>
            </div>
        </section>
        </div>
        <div class="col-lg-1"></div>
    </div>
  </section>
</section>

<script>
function get_bal_amt(emp_name){
    //Ajax
    $("#detail").empty().html('<img src="<?php echo base_url(); ?>assets/images/wait.gif" />');
        
    if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    } else {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    } 

    xmlhttp.onreadystatechange=function(){
        if(xmlhttp.readyState==4 && xmlhttp.status==200){
            document.getElementById('dtl').innerHTML=xmlhttp.responseText;
        }
    }
    
    var queryString="?emp_name="+emp_name;

    xmlhttp.open("GET","<?php echo base_url(); ?>index.php/financec/pc_adv_balamt" + queryString, true);
    xmlhttp.send();
}

//Restricting Only to insert Numbers
function isNumberKey(evt){
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
  	return false;

  return true;
  
}

//Add Row
function addrow(){
	var table = document.getElementById('item_tbl');
	
	var a =  document.getElementById('item_tbl').rows.length;
	var rowCount = a-1;
	
	var row = table.insertRow(a);
	
	var newCell1 = row.insertCell(0);
	newCell1.innerHTML = '<input type="text" id="size" name="size[]" value="" class="form-control" required>';
	
	var newCell1 = row.insertCell(1);
	newCell1.innerHTML = '<input type="text" id="side" name="side[]" value="" class="form-control" required>';

    var newCell1 = row.insertCell(2);
	newCell1.innerHTML = '<input type="text" id="color" name="color[]" value="" class="form-control" required>';

    var newCell1 = row.insertCell(3);
    newCell1.innerHTML = '<input type="text" id="gsm" name="gsm[]" value="" class="form-control" onkeypress="return isNumberKey(event);" required>';
    
    var newCell1 = row.insertCell(4);
    newCell1.innerHTML = '<input type="text" id="print_color" name="print_color[]" value="" class="form-control" required>';
    
    var newCell1 = row.insertCell(5);
    newCell1.innerHTML = '<input type="text" id="loop_color" name="loop_color[]" value=""  class="form-control" required>';
    
    var newCell1 = row.insertCell(6);
    newCell1.innerHTML = '<select id="price_basis" name="price_basis[]" class="form-control" required><option value="KG">KG</option><option value="PCS">PCS</option></select>';
    
    var newCell1 = row.insertCell(7);
    newCell1.innerHTML = '<input type="text" id="price" name="price[]" value="" class="form-control" onkeypress="return isNumberKey(event);" required>';
    
    var newCell1 = row.insertCell(8);
    newCell1.innerHTML = '<input type="text" id="description" name="description[]" value="" class="form-control">';
    
    var newCell1 = row.insertCell(9);
    newCell1.innerHTML = '<input type="file" id="last_bag_pic" name="last_bag_pic[]" value=""  class="form-control">';
	
	var newCell1 = row.insertCell(10);
	newCell1.innerHTML = '<span class="glyphicon glyphicon-remove" style="font-size:15px;color:red;" onclick="deleterow()"></span>';
}

//Delete Row
function deleterow(){	
	var table = document.getElementById('item_tbl');
	var rowCount = table.rows.length;
	table.deleteRow(rowCount -1);
}
</script>
<?php echo "<h2>Sale Order No - ".$so_id."</h2><br>"; ?>
<input type="hidden" id="so_id" name="so_id" value="<?=$so_id;?>" >
                

<div class="form-group" style="text-align:left">
    <label class="col-sm-1">Customer Name</label>
    <div class="col-sm-3"><?=$so_cust_name; ?></div>

    <label class="col-sm-1">Address</label>
    <div class="col-sm-3"><?=$so_cust_add; ?></div>

    <label class="col-sm-1">City</label>
    <div class="col-sm-3"><?=$so_cust_city?></div>
</div>

<div class="form-group" style="text-align:left">
    <label class="col-sm-1">State</label>
    <div class="col-sm-3"><?=$so_cust_state;?></div>

    <label class="col-sm-1">Country</label>
    <div class="col-sm-3"><?=$so_cust_country;?></div>

    <label class="col-sm-1">Pincode</label>
    <div class="col-sm-3"><?=$so_cust_pin?></div>
</div>

<div class="form-group" style="text-align:left">
    <label class="col-sm-1">Email</label>
    <div class="col-sm-3"><?=$so_cust_email?></div>

    <label class="col-sm-1">Phone</label>
    <div class="col-sm-3"><?=$so_cust_phone?></div>
</div>

<div class="form-group">
    <br/><br/>
    <div class="table-wrapper">
        <div class="table-title">
            <div class="row">
                <div class="col-sm-12"><h3>Item Details</b></h3></div>
            </div>
        </div>
        <table class="table table-bordered" id="item_tbl">
            <thead>
                <tr>
                    <th>Size</th>
                    <th>Side</th>
                    <th>Color</th>
                    <th>GSM</th>
                    <th>Print Color</th>
                    <th>Loop Color</th>
                    <th>Price Basis</th>
                    <th>Price</th>
                    <th>Description</th>
                    <th>Last Bag Picture</th>
                </tr>
            </thead>
            <tbody style="text-align:left">
                <?php
                    $sql_itm_list = "select * from so_dtl where so_id ='".$so_id."'";
                    $qry_itm_list = $this->db->query($sql_itm_list);

                    $cnt = 0;
                    foreach($qry_itm_list->result() as $row){
                        $cnt++;
                        $size = $row->size;
                        $side = $row->side;
                        $color = $row->color;
                        $gsm = $row->gsm;
                        $print_color = $row->print_color;
                        $loop_color = $row->loop_color;
                        $price_basis = $row->price_basis;
                        $price = $row->price;
                        $description = $row->description;
                        $last_bag_pic = $row->last_bag_pic;
                ?>
                <tr>
                    <td><?=$size;?></td>
                    <td><?=$side;?></td>
                    <td><?=$color;?></td>
                    <td><?=$gsm;?></td>
                    <td><?=$print_color;?></td>
                    <td><?=$loop_color;?></td>
                    <td><?=$price_basis;?></td>
                    <td><?=$price;?></td>
                    <td><?=$description;?></td>
                    <td><?=$last_bag_pic;?></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
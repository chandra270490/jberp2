<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('sales')){
    
    function so_cnt($so_order_status){
        $ci =& get_instance();
        $ci->load->database();

        $sql="select count(*) as cnt1 from so_mst where so_order_status = '".$so_order_status."'";
		
        $query = $ci->db->query($sql)->row();

        $cnt1 = $query->cnt1;

        return $cnt1;
    }

	function so_det($so_order_status){
		$ci =& get_instance();
        $ci->load->database();

        $sql_stat = "select edit_url from so_status_mst where so_status = '".$so_order_status."'";
        $qry_stat = $ci->db->query($sql_stat)->row();

        $url = $qry_stat->edit_url;
        
        $data = "
        <table class='table table-bordered'>
        <tr style='background-color:#66ccff; font-weight:bold'>
            <td>SO No.</td>
            <td>Customer Name</td>
            <td>City</td>
            <td>State</td>
            <td>Status</td>
            <td>Created By</td>
            <td>Created Date</td>
            <td>Edit</td>
        </tr>";
			
        $sql="select * from so_mst where so_order_status = '".$so_order_status."'";
		
        $query = $ci->db->query($sql);

		foreach ($query->result() as $row) {
		  $so_id = $row->so_id;
          $so_cust_name = $row->so_cust_name;
          $so_cust_city = $row->so_cust_city;
          $so_cust_state = $row->so_cust_state;
          $so_order_status = $row->so_order_status;
          $created_by = $row->created_by;
          $created_date = $row->created_date;

          $data .= '<tr>';
          $data .= '<td>'.$so_id.'</td>';
          $data .= '<td>'.$so_cust_name.'</td>';
          $data .= '<td>'.$so_cust_city.'</td>';
          $data .= '<td>'.$so_cust_state.'</td>';
          $data .= '<td>'.$so_order_status.'</td>';
          $data .= '<td>'.$created_by.'</td>';
          $data .= '<td>'.substr($created_date,0,11).'</td>';
          $data .= '<td><a href="'.base_url().'index.php/'.$url.$so_id.'">Edit</a></td>';

          $data .= '</tr>';

        }
        $data .='</table><br><br>';
		
		return $data;
	}
	
}
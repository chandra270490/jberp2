<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Drc extends CI_Controller {
 
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database(); 
		 $this->load->model('drm');
	}

	//Inquiry
	public function dr_list(){
		$tbl_nm = "dr_mst";
		$data = array();
		$data['list_title'] = "Daily Report List";
		$data['list_url'] = "drc/dr_list";
		$data['tbl_nm'] = "dr_mst";
		$data['primary_col'] = "dr_id";
		$data['edit_url'] = "drc/dr_form";
		$data['edit_enable'] = "yes";
		
		$data['ViewHead'] = $this->drm->ListHead($tbl_nm);

		$where_str = "where dr_created_by ='".$_SESSION['username']."'";

		$data['where_str'] = $where_str;

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Daily Report List' => 'drc/dr_list',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/ListView', $data); 
		$this->load->view('admin/footer');
	}

	public function dr_form(){
		$dr_id = $_REQUEST['id'];
		if($dr_id != ""){
			$data['get_dr_by_id'] = $this->drm->get_dr_by_id($dr_id);
		}
		
		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Daily Report List' => 'drc/dr_list',
			'DR Form' => 'drc/dr_form?id="'.$dr_id.'"',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/dr/dr_form', $data); 
		$this->load->view('admin/footer');	
	}

	public function dr_entry(){
		$data = array();
		$data['dr_entry'] = $this->drm->dr_entry($data);
		$data['message'] = '';
		$data['url'] = 'drc/dr_list';
		$this->load->view('admin/QueryPage', $data); 	
	}
}

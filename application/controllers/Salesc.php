<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Salesc extends CI_Controller {
 
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database(); 
		 $this->load->model('salesm');
    }
    
    //Dashboard
    public function index(){ 
		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Sales' => 'salesc',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/sales/sales_db',$data); 
		$this->load->view('admin/footer');
	}

	//Inquiry
	public function order_list(){
		$tbl_nm = "so_mst";
		$data = array();
		$data['list_title'] = "Orders List";
		$data['list_url'] = "salesc/order_list";
		$data['tbl_nm'] = "so_mst";
		$data['primary_col'] = "so_id";
		$data['edit_url'] = "salesc/order_form";
		$data['edit_enable'] = "yes";
		
		$data['ViewHead'] = $this->salesm->ListHead($tbl_nm);

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Sales' => 'salesc', 
			'Order List' => 'salesc/order_list',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/ListView', $data); 
		$this->load->view('admin/footer');
	}

	public function order_form(){
		$so_id = $_REQUEST['id'];
		if($so_id != ""){
			$data['get_by_id'] = $this->salesm->get_by_id($so_id);
		}
		
		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Sales' => 'salesc',
			'Order Form' => 'salesc/order_form?id="'.$so_id.'"',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/sales/order_form', $data); 
		$this->load->view('admin/footer');	
	}

	public function order_entry(){
		$data = array();
		$data['order_entry'] = $this->salesm->order_entry($data);
		$data['message'] = '';
		$data['url'] = 'salesc/order_stages';
		$this->load->view('admin/QueryPage', $data); 	
	}

	//Order Stages
	public function order_stages(){
		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Sales' => 'salesc',
			'Order Stages' => 'salesc/order_stages',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/sales/order_stages', $data); 
		$this->load->view('admin/footer');	
	}

	public function order_approval(){
		$id = $_REQUEST['id'];
		if($id != ""){
			$data['get_by_id'] = $this->salesm->get_by_id($id);
		}

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Sales' => 'salesc',
			'Order Stages' => 'salesc/order_stages',
			'Order Approval' => 'salesc/order_approval',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/sales/order_approval', $data); 
		$this->load->view('admin/footer');	
	}

	public function order_approval_entry(){
		$data = array();
		$data['order_approval_entry'] = $this->salesm->order_approval_entry($data);
		$data['message'] = '';
		$data['url'] = 'salesc/order_stages';
		$this->load->view('admin/QueryPage', $data); 	
	}

	public function design_making(){
		$id = $_REQUEST['id'];
		if($id != ""){
			$data['get_by_id'] = $this->salesm->get_by_id($id);
		}

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Sales' => 'salesc',
			'Order Stages' => 'salesc/order_stages',
			'Design Making' => 'salesc/design_making',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/sales/design_making', $data); 
		$this->load->view('admin/footer');	
	}

	public function design_making_entry(){
		$data = array();
		$data['design_making_entry'] = $this->salesm->design_making_entry($data);
		$data['message'] = '';
		$data['url'] = 'salesc/order_stages';
		$this->load->view('admin/QueryPage', $data); 	
	}

	public function design_approval(){
		$id = $_REQUEST['id'];
		if($id != ""){
			$data['get_by_id'] = $this->salesm->get_by_id($id);
		}

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Sales' => 'salesc',
			'Order Stages' => 'salesc/order_stages',
			'Design Approval' => 'salesc/design_approval',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/sales/design_approval', $data); 
		$this->load->view('admin/footer');	
	}

	public function design_approval_entry(){
		$data = array();
		$data['design_approval_entry'] = $this->salesm->design_approval_entry($data);
		$data['message'] = '';
		$data['url'] = 'salesc/order_stages';
		$this->load->view('admin/QueryPage', $data); 	
	}

	public function die_making(){
		$id = $_REQUEST['id'];
		if($id != ""){
			$data['get_by_id'] = $this->salesm->get_by_id($id);
		}

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Sales' => 'salesc',
			'Order Stages' => 'salesc/order_stages',
			'Die Making' => 'salesc/die_making',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/sales/die_making', $data); 
		$this->load->view('admin/footer');	
	}

	public function die_making_entry(){
		$data = array();
		$data['die_making_entry'] = $this->salesm->die_making_entry($data);
		$data['message'] = '';
		$data['url'] = 'salesc/order_stages';
		$this->load->view('admin/QueryPage', $data); 	
	}

	public function bag_making(){
		$id = $_REQUEST['id'];
		if($id != ""){
			$data['get_by_id'] = $this->salesm->get_by_id($id);
		}

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Sales' => 'salesc',
			'Order Stages' => 'salesc/order_stages',
			'Bag Making' => 'salesc/bag_making',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/sales/bag_making', $data); 
		$this->load->view('admin/footer');	
	}

	public function bag_making_entry(){
		$data = array();
		$data['bag_making_entry'] = $this->salesm->bag_making_entry($data);
		$data['message'] = '';
		$data['url'] = 'salesc/order_stages';
		$this->load->view('admin/QueryPage', $data); 	
	}

	public function printing(){
		$id = $_REQUEST['id'];
		if($id != ""){
			$data['get_by_id'] = $this->salesm->get_by_id($id);
		}

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Sales' => 'salesc',
			'Order Stages' => 'salesc/order_stages',
			'Printing' => 'salesc/printing',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/sales/printing', $data); 
		$this->load->view('admin/footer');	
	}

	public function printing_entry(){
		$data = array();
		$data['printing_entry'] = $this->salesm->printing_entry($data);
		$data['message'] = '';
		$data['url'] = 'salesc/order_stages';
		$this->load->view('admin/QueryPage', $data); 	
	}

	public function counting_pcs(){
		$id = $_REQUEST['id'];
		if($id != ""){
			$data['get_by_id'] = $this->salesm->get_by_id($id);
		}

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Sales' => 'salesc',
			'Order Stages' => 'salesc/order_stages',
			'Counting PCS' => 'salesc/counting_pcs',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/sales/counting_pcs', $data); 
		$this->load->view('admin/footer');	
	}

	public function counting_pcs_entry(){
		$data = array();
		$data['counting_pcs_entry'] = $this->salesm->counting_pcs_entry($data);
		$data['message'] = '';
		$data['url'] = 'salesc/order_stages';
		$this->load->view('admin/QueryPage', $data); 	
	}

	public function weight_counting(){
		$id = $_REQUEST['id'];
		if($id != ""){
			$data['get_by_id'] = $this->salesm->get_by_id($id);
		}

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Sales' => 'salesc',
			'Order Stages' => 'salesc/order_stages',
			'Weight Counting' => 'salesc/weight_counting',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/sales/weight_counting', $data); 
		$this->load->view('admin/footer');	
	}

	public function weight_counting_entry(){
		$data = array();
		$data['weight_counting_entry'] = $this->salesm->weight_counting_entry($data);
		$data['message'] = '';
		$data['url'] = 'salesc/order_stages';
		$this->load->view('admin/QueryPage', $data); 	
	}

	public function generate_bill(){
		$id = $_REQUEST['id'];
		if($id != ""){
			$data['get_by_id'] = $this->salesm->get_by_id($id);
		}

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Sales' => 'salesc',
			'Order Stages' => 'salesc/order_stages',
			'Generate Bill' => 'salesc/generate_bill',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/sales/generate_bill', $data); 
		$this->load->view('admin/footer');	
	}

	public function generate_bill_entry(){
		$data = array();
		$data['generate_bill_entry'] = $this->salesm->generate_bill_entry($data);
		$data['message'] = '';
		$data['url'] = 'salesc/order_stages';
		$this->load->view('admin/QueryPage', $data); 	
	}

	public function generate_bill_approval(){
		$id = $_REQUEST['id'];
		if($id != ""){
			$data['get_by_id'] = $this->salesm->get_by_id($id);
		}

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Sales' => 'salesc',
			'Order Stages' => 'salesc/order_stages',
			'Generate Bill Approval' => 'salesc/generate_bill_approval',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/sales/generate_bill_approval', $data); 
		$this->load->view('admin/footer');	
	}

	public function generate_bill_approval_entry(){
		$data = array();
		$data['generate_bill_approval_entry'] = $this->salesm->generate_bill_approval_entry($data);
		$data['message'] = '';
		$data['url'] = 'salesc/order_stages';
		$this->load->view('admin/QueryPage', $data); 	
	}

	public function payment(){
		$id = $_REQUEST['id'];
		if($id != ""){
			$data['get_by_id'] = $this->salesm->get_by_id($id);
		}
		
		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Sales' => 'salesc',
			'Order Stages' => 'salesc/order_stages',
			'Payment' => 'salesc/payment',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/sales/payment', $data); 
		$this->load->view('admin/footer');	
	}

	public function payment_entry(){
		$data = array();
		$data['payment_entry'] = $this->salesm->payment_entry($data);
		$data['message'] = '';
		$data['url'] = 'salesc/order_stages';
		$this->load->view('admin/QueryPage', $data); 	
	}

	public function payment_approval(){
		$id = $_REQUEST['id'];
		if($id != ""){
			$data['get_by_id'] = $this->salesm->get_by_id($id);
		}

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Sales' => 'salesc',
			'Order Stages' => 'salesc/order_stages',
			'Payment Approval' => 'salesc/payment_approval',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/sales/payment_approval', $data); 
		$this->load->view('admin/footer');	
	}

	public function payment_approval_entry(){
		$data = array();
		$data['payment_approval_entry'] = $this->salesm->payment_approval_entry($data);
		$data['message'] = '';
		$data['url'] = 'salesc/order_stages';
		$this->load->view('admin/QueryPage', $data); 	
	}

	public function dispatch(){
		$id = $_REQUEST['id'];
		if($id != ""){
			$data['get_by_id'] = $this->salesm->get_by_id($id);
		}

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Sales' => 'salesc',
			'Order Stages' => 'salesc/order_stages',
			'Dispatch' => 'salesc/dispatch',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/sales/dispatch', $data); 
		$this->load->view('admin/footer');	
	}

	public function dispatch_entry(){
		$data = array();
		$data['dispatch_entry'] = $this->salesm->dispatch_entry($data);
		$data['message'] = '';
		$data['url'] = 'salesc/order_stages';
		$this->load->view('admin/QueryPage', $data); 	
	}

	public function dispatch_approval(){
		$id = $_REQUEST['id'];
		if($id != ""){
			$data['get_by_id'] = $this->salesm->get_by_id($id);
		}

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Sales' => 'salesc',
			'Order Stages' => 'salesc/order_stages',
			'Dispatch Approval' => 'salesc/dispatch_approval',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/sales/dispatch_approval', $data); 
		$this->load->view('admin/footer');	
	}

	public function dispatch_approval_entry(){
		$data = array();
		$data['dispatch_approval_entry'] = $this->salesm->dispatch_approval_entry($data);
		$data['message'] = '';
		$data['url'] = 'salesc/order_stages';
		$this->load->view('admin/QueryPage', $data); 	
	}

	public function pod(){
		$id = $_REQUEST['id'];
		if($id != ""){
			$data['get_by_id'] = $this->salesm->get_by_id($id);
		}

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Sales' => 'salesc',
			'Order Stages' => 'salesc/order_stages',
			'POD' => 'salesc/pod',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/sales/pod', $data); 
		$this->load->view('admin/footer');	
	}

	public function pod_entry(){
		$data = array();
		$data['pod_entry'] = $this->salesm->pod_entry($data);
		$data['message'] = '';
		$data['url'] = 'salesc/order_stages';
		$this->load->view('admin/QueryPage', $data); 	
	}

	public function complete_orders(){
		$id = $_REQUEST['id'];
		if($id != ""){
			$data['get_by_id'] = $this->salesm->get_by_id($id);
		}
		
		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Sales' => 'salesc',
			'Order Stages' => 'salesc/order_stages',
			'Complete Orders' => 'salesc/complete_orders',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/sales/complete_orders', $data); 
		$this->load->view('admin/footer');	
	}
}

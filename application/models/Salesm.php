<?php
class Salesm extends CI_Model{  
	function __construct(){   
		parent::__construct();  
	}

	function ListHead($tbl_nm){
		$query = $this->db->query("SHOW columns FROM $tbl_nm");

		return $query;
	}

	public function get_by_id($so_id){
		$query = $this->db->query("select * from so_mst where so_id = '".$so_id."'");
		return $query;
	}

	//SO Entry
	public function order_entry($data){  
		$so_id = $this->input->post("so_id");
		$so_id1 = $this->input->post("so_id");
		$so_cust_name = $this->input->post("so_cust_name");
		$so_cust_add = $this->input->post("so_cust_add");
		$so_cust_city = $this->input->post("so_cust_city");
		$so_cust_state = $this->input->post("so_cust_state");
		$so_cust_country = $this->input->post("so_cust_country");
		$so_cust_pin = $this->input->post("so_cust_pin");
		$so_cust_email = $this->input->post("so_cust_email");
		$so_cust_phone = $this->input->post("so_cust_phone");
		$so_order_value = $this->input->post("so_order_value");
		$so_order_status = "Fresh";
		$created_by = $_SESSION['username'];
		$created_date = date("Y-m-d h:i:s");
		$modified_by = $_SESSION['username'];

		//Details
		$size = $this->input->post("size");
		$side = $this->input->post("side");
		$color = $this->input->post("color");
		$gsm = $this->input->post("gsm");
		$print_color = $this->input->post("print_color");
		$loop_color = $this->input->post("loop_color");
		$price_basis = $this->input->post("price_basis");
		$price = $this->input->post("price");
		$description = $this->input->post("description");
		$last_bag_pic = $this->input->post("last_bag_pic");
		$so_order_status = "Order Approval";
		
		$arr_cnt = count($size);
		
		//Transaction Start
		$this->db->trans_start();

		if($so_id1 == ""){
			//Insert Code
			$sql = "insert into so_mst(so_cust_name, so_cust_add, so_cust_city, so_cust_state, 
			so_cust_country, so_cust_pin, so_cust_email, so_cust_phone, 
			so_order_value, so_order_status, created_by, created_date, modified_by) 
			values 
			('".$so_cust_name."', '".$so_cust_add."', '".$so_cust_city."', '".$so_cust_state."', 
			'".$so_cust_country."', '".$so_cust_pin."', '".$so_cust_email."', '".$so_cust_phone."', 
			'".$so_order_value."', '".$so_order_status."', '".$created_by."', '".$created_date."', '".$modified_by."')";

			$this->db->query($sql);

			for($i=0; $i<$arr_cnt; $i++){

				$sql_max_id = "select max(so_id) as so_id from so_mst";
				$qry_max_id = $this->db->query($sql_max_id)->row();
				$so_id_new = $qry_max_id->so_id;

				$sql_itm_ins = "insert into so_dtl(so_id, size, side, color, 
				gsm, print_color, loop_color, price_basis, price, 
				description, last_bag_pic, created_by, created_date, modified_by) 
				values ('".$so_id_new."', '".$size[$i]."', '".$side[$i]."', '".$color[$i]."', 
				'".$gsm[$i]."', '".$print_color[$i]."', '".$loop_color[$i]."', '".$price_basis[$i]."', '".$price[$i]."', 
				'".$description[$i]."', '".$last_bag_pic[$i]."', '".$created_by."', '".$created_date."', '".$modified_by."')";

				$qry_itm_ins = $this->db->query($sql_itm_ins);
			}

		} else {
			//Update Code
			$sql = "update so_mst set so_cust_name = '".$so_cust_name."', so_cust_add = '".$so_cust_add."', 
			so_cust_city = '".$so_cust_city."', so_cust_state = '".$so_cust_state."', 
			so_cust_country = '".$so_cust_country."', so_cust_pin = '".$so_cust_pin."', 
			so_cust_email = '".$so_cust_email."', so_cust_phone = '".$so_cust_phone."', 
			so_order_value = '".$so_order_value."', so_order_status = '".$so_order_status."', 
			modified_by = '".$modified_by."'
			where so_id = '".$so_id."'";

			$this->db->query($sql);

			$sql_itm_cnt = "select count(*) as cnt from so_dtl where so_id = '".$so_id."'";
			$qry_itm_cnt = $this->db->query($sql_itm_cnt)->row();
			$cnt = $qry_itm_cnt->cnt;

			if($cnt > 0){
				$sql_itm_del = "delete from so_dtl where so_id = '".$so_id."'";
				$qry_itm_del = $this->db->query($sql_itm_del);

				for($i=0; $i<$arr_cnt; $i++){
	
					$sql_itm_ins = "insert into so_dtl(so_id, size, side, color, 
					gsm, print_color, loop_color, price_basis, price, 
					description, last_bag_pic, created_by, created_date, modified_by) 
					values ('".$so_id."', '".$size[$i]."', '".$side[$i]."', '".$color[$i]."', 
					'".$gsm[$i]."', '".$print_color[$i]."', '".$loop_color[$i]."', '".$price_basis[$i]."', '".$price[$i]."', 
					'".$description[$i]."', '".$last_bag_pic[$i]."', '".$created_by."', '".$created_date."', '".$modified_by."')";
	
					$qry_itm_ins = $this->db->query($sql_itm_ins);
				}
			} else {
				for($i=0; $i<$arr_cnt; $i++){

					$sql_itm_ins = "insert into so_dtl(so_id, size, side, color, 
					gsm, print_color, loop_color, price_basis, price, 
					description, last_bag_pic, created_by, created_date, modified_by) 
					values ('".$so_id."', '".$size[$i]."', '".$side[$i]."', '".$color[$i]."', 
					'".$gsm[$i]."', '".$print_color[$i]."', '".$loop_color[$i]."', '".$price_basis[$i]."', '".$price[$i]."', 
					'".$description[$i]."', '".$last_bag_pic[$i]."', '".$created_by."', '".$created_date."', '".$modified_by."')";
	
					$qry_itm_ins = $this->db->query($sql_itm_ins);

				}
			}

		}

		$this->db->trans_complete();
		//Transanction Complete
	 }

	 public function order_approval_entry($data){  
		$so_id = $this->input->post("so_id");
		$so_order_app_rmks = $this->input->post("so_order_app_rmks");
		$so_order_status = "Design Making";
		$created_by = $_SESSION['username'];
		$created_date = date("Y-m-d h:i:s");
		$modified_by = $_SESSION['username'];
		
		//Transaction Start
		$this->db->trans_start();
		
		//Update Code
		$sql = $this->db->query("update so_mst set so_order_app_rmks = '".$so_order_app_rmks."', so_order_status = '".$so_order_status."',
		so_order_app_by = '".$created_by."', so_order_app_date = '".$created_date."', 
		modified_by = '".$modified_by."' where so_id = '".$so_id."'");

		$this->db->trans_complete();
		//Transanction Complete
	 }

	 public function design_making_entry($data){  
		$so_id = $this->input->post("so_id");
		$so_design_comp = $this->input->post("so_design_comp");
		$so_order_status = "Design Approval";
		$created_by = $_SESSION['username'];
		$created_date = date("Y-m-d h:i:s");
		$modified_by = $_SESSION['username'];
		
		//Transaction Start
		$this->db->trans_start();
		
		//Update Code
		$sql = $this->db->query("update so_mst set so_design_comp = '".$so_design_comp."', so_order_status = '".$so_order_status."',
		so_design_by = '".$created_by."', so_design_date = '".$created_date."', 
		modified_by = '".$modified_by."' where so_id = '".$so_id."'");

		$this->db->trans_complete();
		//Transanction Complete
	 }

	 public function design_approval_entry($data){  
		$so_id = $this->input->post("so_id");
		$so_design_app_rmks = $this->input->post("so_design_app_rmks");
		$so_order_status = "Die Making";
		$created_by = $_SESSION['username'];
		$created_date = date("Y-m-d h:i:s");
		$modified_by = $_SESSION['username'];
		
		//Transaction Start
		$this->db->trans_start();
		
		//Update Code
		$sql = $this->db->query("update so_mst set so_design_app_rmks = '".$so_design_app_rmks."', so_order_status = '".$so_order_status."',
		so_design_app_by = '".$created_by."', so_design_app_date = '".$created_date."', 
		modified_by = '".$modified_by."' where so_id = '".$so_id."'");

		$this->db->trans_complete();
		//Transanction Complete
	 }

	 public function die_making_entry($data){  
		$so_id = $this->input->post("so_id");
		$so_die_comp = $this->input->post("so_die_comp");
		$so_order_status = "Bag Making";
		$created_by = $_SESSION['username'];
		$created_date = date("Y-m-d h:i:s");
		$modified_by = $_SESSION['username'];
		
		//Transaction Start
		$this->db->trans_start();
		
		//Update Code
		$sql = $this->db->query("update so_mst set so_die_comp = '".$so_die_comp."', so_order_status = '".$so_order_status."',
		so_die_by = '".$created_by."', so_die_date = '".$created_date."', 
		modified_by = '".$modified_by."' where so_id = '".$so_id."'");

		$this->db->trans_complete();
		//Transanction Complete
	 }

	 public function bag_making_entry($data){  
		$so_id = $this->input->post("so_id");
		$so_bag_comp = $this->input->post("so_bag_comp");
		$so_order_status = "Printing";
		$created_by = $_SESSION['username'];
		$created_date = date("Y-m-d h:i:s");
		$modified_by = $_SESSION['username'];
		
		//Transaction Start
		$this->db->trans_start();
		
		//Update Code
		$sql = $this->db->query("update so_mst set so_bag_comp = '".$so_bag_comp."', so_order_status = '".$so_order_status."',
		so_bag_by = '".$created_by."', so_bag_date = '".$created_date."', 
		modified_by = '".$modified_by."' where so_id = '".$so_id."'");

		$this->db->trans_complete();
		//Transanction Complete
	 }

	 public function printing_entry($data){  
		$so_id = $this->input->post("so_id");
		$so_printing_comp = $this->input->post("so_printing_comp");
		$so_order_status = "Counting PCS";
		$created_by = $_SESSION['username'];
		$created_date = date("Y-m-d h:i:s");
		$modified_by = $_SESSION['username'];
		
		//Transaction Start
		$this->db->trans_start();
		
		//Update Code
		$sql = $this->db->query("update so_mst set so_printing_comp = '".$so_printing_comp."', so_order_status = '".$so_order_status."',
		so_printing_by = '".$so_printing_by."', so_printing_date = '".$so_printing_date."', 
		modified_by = '".$modified_by."' where so_id = '".$so_id."'");

		$this->db->trans_complete();
		//Transanction Complete
	 }

	 public function counting_pcs_entry($data){  
		$so_id = $this->input->post("so_id");
		$so_nop = $this->input->post("so_nop");
		$so_order_status = "Weight Counting";
		$created_by = $_SESSION['username'];
		$created_date = date("Y-m-d h:i:s");
		$modified_by = $_SESSION['username'];
		
		//Transaction Start
		$this->db->trans_start();
		
		//Update Code
		$sql = $this->db->query("update so_mst set so_nop = '".$so_nop."', so_order_status = '".$so_order_status."',
		so_pcs_cnt_by = '".$created_by."', so_pcs_cnt_date = '".$created_date."', 
		modified_by = '".$modified_by."' where so_id = '".$so_id."'");

		$this->db->trans_complete();
		//Transanction Complete
	 }

	 public function weight_counting_entry($data){  
		$so_id = $this->input->post("so_id");
		$so_weight = $this->input->post("so_weight");
		$so_order_status = "Generate Bill";
		$created_by = $_SESSION['username'];
		$created_date = date("Y-m-d h:i:s");
		$modified_by = $_SESSION['username'];
		
		//Transaction Start
		$this->db->trans_start();
		
		//Update Code
		$sql = $this->db->query("update so_mst set so_weight = '".$so_weight."', so_order_status = '".$so_order_status."',
		so_weight_by = '".$created_by."', so_weight_date = '".$created_date."', 
		modified_by = '".$modified_by."' where so_id = '".$so_id."'");

		$this->db->trans_complete();
		//Transanction Complete
	 }

	 public function generate_bill_entry($data){  
		$so_id = $this->input->post("so_id");
		$so_order_status = "Generate Bill Approval";
		$created_by = $_SESSION['username'];
		$created_date = date("Y-m-d h:i:s");
		$modified_by = $_SESSION['username'];
		
		//Transaction Start
		$this->db->trans_start();
		
		//Update Code
		$sql = $this->db->query("update so_mst set so_order_status = '".$so_order_status."',
		so_gb_by = '".$created_by."', so_gb_date = '".$created_date."', 
		modified_by = '".$modified_by."' where so_id = '".$so_id."'");

		$this->db->trans_complete();
		//Transanction Complete
	 }

	 public function generate_bill_approval_entry($data){  
		$so_id = $this->input->post("so_id");
		$so_gb_app_rmks = $this->input->post("rmks");
		$so_order_status = "Payment";
		$created_by = $_SESSION['username'];
		$created_date = date("Y-m-d h:i:s");
		$modified_by = $_SESSION['username'];
		
		//Transaction Start
		$this->db->trans_start();
		
		//Update Code
		$sql = $this->db->query("update so_mst set so_order_status = '".$so_order_status."', so_gb_app_rmks = '".$so_gb_app_rmks."',
		so_gb_app_by = '".$created_by."', so_gb_app_date = '".$created_date."', 
		modified_by = '".$modified_by."' where so_id = '".$so_id."'");

		$this->db->trans_complete();
		//Transanction Complete
	 }

	 public function payment_entry($data){  
		$so_id = $this->input->post("so_id");
		$so_order_status = "Payment Approval";
		$created_by = $_SESSION['username'];
		$created_date = date("Y-m-d h:i:s");
		$modified_by = $_SESSION['username'];
		
		//Transaction Start
		$this->db->trans_start();
		
		//Update Code
		$sql = $this->db->query("update so_mst set so_order_status = '".$so_order_status."',
		so_pay_by = '".$created_by."', so_pay_date = '".$created_date."', 
		modified_by = '".$modified_by."' where so_id = '".$so_id."'");

		$this->db->trans_complete();
		//Transanction Complete
	 }

	 public function payment_approval_entry($data){  
		$so_id = $this->input->post("so_id");
		$so_pay_app_rmks = $this->input->post("rmks");
		$so_order_status = "Dispatch";
		$created_by = $_SESSION['username'];
		$created_date = date("Y-m-d h:i:s");
		$modified_by = $_SESSION['username'];
		
		//Transaction Start
		$this->db->trans_start();
		
		//Update Code
		$sql = $this->db->query("update so_mst set so_order_status = '".$so_order_status."', so_pay_app_rmks = '".$so_pay_app_rmks."',
		so_pay_app_by = '".$created_by."', so_pay_app_date = '".$created_date."', 
		modified_by = '".$modified_by."' where so_id = '".$so_id."'");

		$this->db->trans_complete();
		//Transanction Complete
	 }

	 public function dispatch_entry($data){  
		$so_id = $this->input->post("so_id");
		$so_order_status = "Dispatch Approval";
		$created_by = $_SESSION['username'];
		$created_date = date("Y-m-d h:i:s");
		$modified_by = $_SESSION['username'];
		
		//Transaction Start
		$this->db->trans_start();
		
		//Update Code
		$sql = $this->db->query("update so_mst set so_order_status = '".$so_order_status."',
		so_dis_by = '".$created_by."', so_dis_date = '".$created_date."', 
		modified_by = '".$modified_by."' where so_id = '".$so_id."'");

		$this->db->trans_complete();
		//Transanction Complete
	 }

	 public function dispatch_approval_entry($data){  
		$so_id = $this->input->post("so_id");
		$so_pay_app_rmks = $this->input->post("rmks");
		$so_order_status = "POD";
		$created_by = $_SESSION['username'];
		$created_date = date("Y-m-d h:i:s");
		$modified_by = $_SESSION['username'];
		
		//Transaction Start
		$this->db->trans_start();
		
		//Update Code
		$sql = $this->db->query("update so_mst set so_order_status = '".$so_order_status."', so_dis_app_rmks = '".$so_pay_app_rmks."',
		so_dis_app_by = '".$created_by."', so_dis_app_date = '".$created_date."', 
		modified_by = '".$modified_by."' where so_id = '".$so_id."'");

		$this->db->trans_complete();
		//Transanction Complete
	 }

	 public function pod_entry($data){  
		$so_id = $this->input->post("so_id");
		$so_order_status = "Complete";
		$created_by = $_SESSION['username'];
		$created_date = date("Y-m-d h:i:s");
		$modified_by = $_SESSION['username'];
		
		//Transaction Start
		$this->db->trans_start();
		
		//Update Code
		$sql = $this->db->query("update so_mst set so_order_status = '".$so_order_status."',
		so_pod_by = '".$created_by."', so_pod_date = '".$created_date."', 
		modified_by = '".$modified_by."' where so_id = '".$so_id."'");

		$this->db->trans_complete();
		//Transanction Complete
	 }
}  
?>